resource "google_compute_instance" "app" {
  name         = var.name
  machine_type = var.machine_type
  zone         = var.zone

  boot_disk {
    initialize_params {
      image = var.image
      size  = var.size
      type  = var.type
    }
  }

  network_interface {
    network = var.network

    access_config {
    }
  }

    provisioner "remote-exec" {
    inline = ["echo 'Wait until SSH is ready'"]

    connection {
      type        = "ssh"
      user        = var.ssh_user
      private_key = file(var.private_key_path)
      host        = google_compute_instance.app.network_interface.0.access_config.0.nat_ip
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook  -i ${google_compute_instance.app.network_interface.0.access_config.0.nat_ip}, --private-key ${var.private_key_path} webserver.yaml"
  }
}

resource "google_compute_firewall" "web" {
  name    = var.f_name
  network = var.network

  allow {
    protocol = var.protocol
    ports    = ["80", "8080", "8000"]
  }
  source_ranges = ["0.0.0.0/0"]
}

