variable "name" {
  type = string
}
variable "machine_type" {
  type = string
}
variable "network" {
  type = string
}
variable "zone" {
  type = string
}
variable "image" {
  type = string
}
variable "type" {
  type = string
}
variable "size" {
  type = number
}
//firewall
variable "f_name" {
  type = string
}
variable "protocol" {
  type = string
}
//ansible
variable "ssh_user" {
  type = string
}
variable "private_key_path" {
  type = string
  default = "~/.ssh/ansbile_ed25519"
}