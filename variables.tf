variable "project" {
  type    = string
  default = ""
}
variable "credentials_file" {
  type    = string
  default = ""
}
variable "region" {
  type    = string
  default = "us-central1"
}