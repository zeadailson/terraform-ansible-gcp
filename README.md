# terraform-ansible-gcp

Prerequisites

Ansible [core 2.12.8]
Terraform ~> 1.0

Generate SSH Key Pair

Run the following command to generate elliptic curve ssh key pair

ssh-keygen -t ed25519 -f ~/.ssh/ansible_ed25519 -C ansible

Optionally, you can generate RSA key pair

ssh-keygen -t rsa -f ~/.ssh/ansible -C ansible -b 2048

Add SSH Key to GCP Project

Go to compute engine section

Click Metadata
Select SSH KEYS tab and click ADD SSH KEY

Upload public ansible_ed25519.pub key

cat ~/.ssh/ansible_ed25519.pub

Ansible Playbook for Webserver Installation

webserver.yaml playbook
Ansible config ansible.cfg to skip host key verification

Terraform Ansible Integration Example
Pass parameters directly in main.tf file in root folder
GCE module with environment variables, when passing the main.tf settings in the root, this information is loaded in the module variables

Mandatory Parameters

project
credentials_file