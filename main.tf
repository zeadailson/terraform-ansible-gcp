module "app" {
  source       = "./modules/gce"
  name         = "webserver"
  machine_type = "f1-micro"
  zone         = "us-central1-a"
  size         = "10"
  type         = "pd-standard"
  image        = "debian-cloud/debian-11"
  network      = "default"
  //firewall
  f_name   = "web-access"
  protocol = "tcp"
  //ansible
  ssh_user = "ansible"
}
